#  CentOS/OracleLinux Universal Forwarder 

This Role will deploy the Splunk Universal Forwarder on CentOS 7 & Oracle Linux 8 hosts. A broader overview of Universal Forwarders in other Linux distributions can be found below.

- https://docs.splunk.com/Splexicon:Universalforwarder
- https://docs.splunk.com/Documentation/Splunk/8.0.4/Installation/InstallonLinux
- https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/collectdSourceCommands
- https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/ManageAgents


## Requirements
This role works on CentOS 7/8 & RedHat derivatives.


## Role Variables
```
splunk_forwarder_user: splunk
splunk_forwarder_group: splunk
splunk_forwarder_uid: 1100
splunk_forwarder_gid: 1110
splunk_tar_checksum: md5:8ce61af6535cda3b90d1d996e05cb4b9
splunk_uri_dom1:  targetUri = domain1.com:8089
splunk_uri_dom2:  targetUri = domain2.com:8089
splunk_uri_dom3:  targetUri = domain3.com:8089
splunk_uri_dom4:  targetUri = domain4.com:8089
splunk_uri_dom5: targetUri = domain5.com:8089
splunk_uri_dom6: targetUri = domain6.com:8089
splunk_uri_dom7: targetUri = domain6.com:8089
usr_id: ME
action_completed: Universal Forwarder installed
```

## Components
Tasks are ran sequentionally in [main.yml](https://gitlab.com/planforfailure/ansible-universal-forwarder/-/blob/master/roles/ansible-universal-forwarder/tasks/main.yml)


## Dependencies
You must have a Splunk indexer running.


## Summary

Modify the following to suit your requirements.

`$ ansible-playbook --extra-vars '@passwd.yml' deploy_uf.yml -bK`

```yaml
---
- hosts:
  - all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-universal-forwarder

  post_tasks:
    - name: Confirm splunk is open on 8089
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/port_check.yml

    - name: Confirm splunkd is running
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/splunk_running.yml

    - name: Update CHANGELOG after deploying Universal Forwarder successfully
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/changelog_update.yml
```
**NOTE:** To remove the Universal Forwarder see [universal-forwarder-removal](https://gitlab.com/planforfailure/ansible-universal-forwarder-removal/-/blob/master/README.md)

## Licence

MIT/BSD
