#  CentOS/Oracle Linux Universal Forwarder

This Role will deploy the Splunk Universal Forwarder on CentOS 7 & Oracle Linux 8 hosts. A broader overview of Universal Forwarders in other Linux distributions can be found below.

https://docs.splunk.com/Splexicon:Universalforwarder
https://docs.splunk.com/Documentation/Splunk/8.0.4/Installation/InstallonLinux
https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/collectdSourceCommands
https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/ManageAgents


#### Requirements
This role works on CentOS 7/8 & RedHat derivatives, however due to issues with collectd package versions which are unavailable for the Universal Forwarder to work as expected, it is deprecated for CentOS 6 derivatives.


#### Role Variables
```
splunk_forwarder_user: splunk
splunk_forwarder_group: splunk
splunk_forwarder_uid: 1100
splunk_forwarder_gid: 1110
splunk_tar_checksum: md5:8ce61af6535cda3b90d1d996e05cb4b9
splunk_uri_domain1:  targetUri = domain1.net:8089
splunk_uri_domain2:  targetUri = domain2.net:8089
splunk_uri_domain3:  targetUri = domain3.net:8089
splunk_uri_domain4:  targetUri = domain4.net:8089
splunk_uri_domain5:  targetUri = domain5.net:8089
usr_id: ME
action_completed: Universal Forwarder installed
```

#### Components
All tasks are ran sequentionally utilising facts on the target host.


#### Dependencies
You must have a Splunk indexer running.


#### Summary

Modify the following playbook as per your requirements

`$ ansible-playbook -i inventory deploy_uf.yml -K`

```yaml
---
- hosts:
  - all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-universal-forwarder

  post_tasks:
    - name: Confirm splunk is open on 8089
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/port_check.yml

    - name: Confirm splunkd is running
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/splunk_running.yml

    - name: Update CHANGELOG after deploying Universal Forwarder successfully
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/changelog_update.yml
      
    - name: Fix splunk app permissions
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/app_perms.yml

    - name: Add splunk group to svc_ansible_splunk
      ansible.builtin.include: roles/ansible-universal-forwarder/tasks/extra_group.yml      
```
To remove the Universal Forwarder from the target host(s) run the following, ensuring you amend the `usr_id` & `action_completed` to reflect the removal for auditing purposes.

`$ ansible-playbook -i inventory remove_uf.yml -K`
